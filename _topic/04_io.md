---
slug: io
title: Input & Output (IO)
---

## Parse, search and manipulated formatted text like HTML and XML
http://www.crummy.com/software/BeautifulSoup/

## Web scraping with Beautiful Soup
http://www.pythonforbeginners.com/beautifulsoup/web-scraping-with-beautifulsoup

## How can I write/edit a csv file based on conditions
http://www.reddit.com/r/learnpython/comments/34g4yo/how_can_i_writeedit_a_csv_file_based_on_conditions/

## How do I handle passwords?
http://www.reddit.com/r/learnpython/comments/32kinl/how_do_i_handle_passwords/

## Reading and Writing Files
http://automatetheboringstuff.com/chapter8/

## Organizing Files
http://automatetheboringstuff.com/chapter9/

## Web Scraping
http://automatetheboringstuff.com/chapter11/

## Working with Excel Spreadsheets
http://automatetheboringstuff.com/chapter12/

## Working with PDF and Word Documents
http://automatetheboringstuff.com/chapter13/

## Working with CSV Files and JSON Data
http://automatetheboringstuff.com/chapter14/

## Sending Email and Text Messages
http://automatetheboringstuff.com/chapter16/

## What databases are and what they are used for ?
* http://people.scs.carleton.ca/~achan/teaching/comp1001/notes/COMP1001-06.pdf
* https://dzone.com/articles/what-are-databases-used-for

## Data Analysis and format performance
* http://statmath.wu.ac.at/courses/data-analysis/itdtHTML/node56.html#SECTION001154000000000000000
* https://www.svds.com/how-to-choose-a-data-format/
* https://afit-r.github.io/tidyr
* https://cran.r-project.org/web/packages/msgpack/vignettes/comparison.html#r-serialization  

## Data Storage  
* http://statmath.wu.ac.at/courses/data-analysis/itdtHTML/node51.html  
