---
layout: page
title: Topics
permalink: /topic/
---

We focus on five topic areas:

 - Project Organization: What are required to accomplish in your project? What are the pieces that you'll use to do so? How should you arrange them? How will they work together? How can those choices help (or hinder!) you plan to get work done?
 - Workspace Tools: What tools should you use? Are there some you have to use? How do they help you work with collaborators? Do they demand too much for those collaborators? Do they comply with your project requirements, like data privacy?
 - Reuse & Reusability: Are there parts of your project that someone else (maybe even your past self!) has already implemented? Are there parts that someone else (hopefully, future you!) will want to use? What should you do to make your code more friendly to including other work, and being included elsewhere?
 - Input & Output: What kind of data do you have? What format should you support reading in? Storing output in? What lines in your code currently should be input instead?
 - High Performance Computing: Where is your project stumbling? Too much data, inefficient implementations? If everything is highly tuned, and it's still too much for your personal machine, what next? How you can break up your problem in bits that will work in parallel? How you can write code that will be cluster-computation ready?
