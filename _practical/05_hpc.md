---
slug: hpc
title: High Performance Computing
---

## Recap:

In the discussion session, we covered the following ideas:

- how does the HPC computing model differ from computing on your local machine?  
- what do those differences mean for how you arrange your program?
- the IO choices you make
- the different categories HPC infrastructures: clusters, and clouds 
- jargon / terminology: multi-cores, GPUs, cloud computing, MPI, etc
- HPC programming languages: CUDAC, Fortran, etc

## Objectives

By the end of this session participants shall be able to:

- identify what is parallelizable in a code
- see the benefits of parallelization from runtimes
- show understanding of IO choices to be made
- appreciate the use of GPUs
- demonstrate knowledge of hpc management systems
- do code testings and prototyping 
- demonstrate knowledge of hpc service providers

## Activities
- counting words in a document
    - estimate time it will take one person by having one student count words in a page
    - estimate how much it will take for a group of 2 students, then 3 students, etc
- identifying parallelizable parts of the code below
```{pseudocode}
INPUTS: k, N, a vector of m values, number of rializations
OUTPUTS: k, N, the vector of m values, error vector
for i = 1:number of m values
    pick ith m value from the vector of m values
    generate a k sparse signal x of dimension N
    for j = 1:number of relizations
        generate an m x N Gaussian matrix, say A
        generate observations y = Ax
        run a reconstruction algorithm to recovery xhat from y, given A
        measure and store the recovery error (difference between x and xhat)
    end for
    take and store the mean/median of the error as the ith error value
end for
return error vector, k, N, and m values
```
- job submission, queuing, and management on CHPC
    - take 5 mins to read the document about this
    - Emmanuel makes a quick demo on this
- code testing / prototyping on a local machine / an interactive node
    - test running the MATLAB code for pseudocode above (`seams.m` in SEAMS Google Drive directory)
    - changes in the inputs and outputs
    - changing syntax like "for" to "parfor" in the MATLAB code above
    - what other changes needed for multi-threading do you know?
- runtime comparison of a serial code and its parallelized version
    - convert the MATLAB code to the language of your choice
    - add timers to the code to measure run times
    - read about how to parallelize the for loop in your language of choice
    - modify your code accordingly 
    - test run it on your multi-core laptop and compare runtimes with the serial implementation
    - test run it on an interactive node on CHPC and compare times with the serial implementation
- multi-cores vs GPUs
    - create Google Colab accounts at (https://colab.research.google.com)
    - read the `welcome page` of your Colab account and familairise yourself with Colab
    - move `LSTM python code` and `csv file` in the SEAMS Google Drive directory
    - run the `LSTM cell` and observe the times
    - find out how to add GPUs and re-run your `LSTM cell` and observe the new runtimes

# Links to data

 - [steps for work on CHPC](https://drive.google.com/open?id=1IAJHIziioVuHLDvZQml6NE0U1YVz8zQ8)
 - [data for practical](https://drive.google.com/open?id=1iVEebO5gyTcgNYybSZo-aeWgmZcIXaSj)
