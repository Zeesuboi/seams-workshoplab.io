---
slug: hpc
title: High Performance Computing
---

[Slides](https://docs.google.com/presentation/d/1_39HFIZZnlEfVV9xLQHlVOGp8k2twCAJ9w0L9Q2e1ps/present).

1. Computer anatomy
    * CPU, RAM, HDD
    * Desktop vs. cluster
2. Why do programs run slowly?
    * Hardware bottlenecks
    * Algorithmic complexity
3. How do you know why a program is slow?
    * System utilities: time, top, and profiling tools
    * Code/algorithm analysis
4. Parallelizeable problems
    * Monte Carlo
    * split-apply-combine
    * Amdahl's Law
    * Race conditions
5. HPC environments
    * multithreading
    * clusters/supercomputers
    * cloud/grid computing
    * scheduling jobs
6. Relevant technologies
    * Shared vs. distributed memory
    * OpenMP, MPI, other language-specific libraries
    * Hadoop, MapReduce

7. GPUs and related terminology
    * Single precision computation
    * Double precision computation
    * Memory management in GPU
    * Languages for GPUs - CUDA-C, Python, ...